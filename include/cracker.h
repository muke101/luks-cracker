#ifndef CRACKER_H_
#define CRACKER_H_

#include <pthread.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include "parser.h"

#define BUFFER 1000

struct keyslot_password	{
	unsigned keyslot_index;
	char *password;
};

struct  keyslot_password *crack(struct phdr header, char *wordlist, unsigned long password_number, unsigned thread_number, unsigned number_of_keyslots);

struct T	{
	pthread_t id;
	char *wordlist_indx;
	unsigned step;
	struct phdr header;
	char *wordlist;
	unsigned keyslot;
	void *result;
};

void *begin_brute_force(void *thread);

#endif
