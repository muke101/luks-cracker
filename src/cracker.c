#include "parser.h"
#include "crypto_backend.h"
#include "cracker.h"

int password_found;

char *map_wordlist(char *filename, size_t *size)	{
	struct stat s;

	int fd = fileno(fopen(filename, "r"));
	fstat(fd, &s);
	*size = s.st_size;

	return mmap(NULL, *size, PROT_READ, MAP_PRIVATE, fd, 0);
}

struct keyslot_password *crack(struct phdr header, char *wordlist, unsigned long password_number, unsigned thread_number, unsigned number_of_keyslots)	{
	unsigned i, j;
	unsigned long passwords_per_thread, remainder;
	size_t size;
	char *mapped_wordlist;
	
	passwords_per_thread = password_number/thread_number; //integer devision, will just mean the last thread takes on a couple more than the others if there are remainders
	remainder = password_number % thread_number;

	mapped_wordlist = map_wordlist(wordlist, &size);

	struct keyslot_password *passwords = calloc(number_of_keyslots, sizeof(struct keyslot_password));
	struct T threads[thread_number];

	for (i=0; i < number_of_keyslots; i++)	{

		password_found = 0;
		passwords[i].keyslot_index = i;

		for (j=0; j < thread_number; j++)	{
			threads[j].wordlist_indx = mapped_wordlist+(j*passwords_per_thread);
			threads[j].header = header;
			threads[j].wordlist = mapped_wordlist;
			threads[j].step = passwords_per_thread;
			threads[j].keyslot = i;
		}
		threads[--j].step+=remainder;

		for (j=0; j < thread_number; j++)	{
			pthread_create(&(threads[j].id), NULL, begin_brute_force, &(threads[j])); 
		}

		for(j=0; j < thread_number; j++)	{
			pthread_join(threads[j].id, &(threads[j].result));
		}

		for (j=0; j < thread_number; j++)
			if (threads[j].result != NULL)
				passwords[i].password = (char *)threads[j].result;
	}

	munmap(mapped_wordlist, size);

	return passwords;

}

void get_pass(char **start, char *password)	{
	char *end;
	end = strchr(*start, '\n');
	if (!end)
		end = strchr(*start, '\0');
	memcpy(password, *start, end-(*start));
	password[end-(*start)] = '\0';
	*start = ++end;
}

void *begin_brute_force(void *thread_info)	{
	unsigned i;
	char password[BUFFER];

	struct T *thread = (struct T *)thread_info;
	int keyslot = thread->keyslot;
	unsigned char *enc_key = thread->header.active_key_slots[keyslot]->key_data;
	unsigned split_length = thread->header.active_key_slots[keyslot]->stripes*thread->header.key_length;
	unsigned char derived_key[thread->header.key_length];
	unsigned block_count = split_length/SECTOR_SIZE; 
	unsigned char split_key[split_length]; 
	unsigned char key_candidate[thread->header.key_length];

	unsigned iv_len = 16;
	unsigned char iv[iv_len];
	unsigned char payload_iv[iv_len];
	memset(payload_iv, 0, iv_len); //sector number for the bulk data begins at sector 0, not the payload offset secotr
	
	for (i=0; i < thread->step && !password_found; i++)	{
		get_pass(&(thread->wordlist_indx), password);
		derive_key(password, strlen(password), thread->header, keyslot, derived_key); 
		decrypt_blocks(block_count, SECTOR_SIZE, iv, iv_len, derived_key, enc_key, split_key);
		af_merge(key_candidate, split_key, thread->header.key_length, thread->header.active_key_slots[keyslot]->stripes, thread->header.version); 
		if (test_entropy(key_candidate, payload_iv, thread->header.test_data) && checksum(key_candidate, thread->header))	{ //condition will stop evaluating if entropy test fails, so long checksum operation only takes place for very likely password candidates
			password_found = 1;
			unsigned char *successful_password = malloc(strlen(password));
			memcpy(successful_password, password, strlen(password));
			printf("password for keyslot %d found: %s\n\n", ++keyslot, password);
			pthread_exit((void *)successful_password);
		}
	}

	pthread_exit(NULL);
}
